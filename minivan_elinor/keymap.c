#include QMK_KEYBOARD_H

extern keymap_config_t keymap_config;

// Each layer gets a name for readability, which is then used in the keymap matrix below.
// The underscores don't mean anything - you can have a layer called STUFF or any other name.
// Layer names don't all need to be of the same length, obviously, and you can also skip them
// entirely and just use numbers.

#define _QWERTY 0
#define _NUMBERS 1
#define _SYMBOLS 2
#define _GREEK 3
#define _ARROWS 4
#define _MEDIA 5
#define _RESET 6

// Macros list

enum custom_keycodes {
  PRTSCR = SAFE_RANGE,
  MATH,
  H_MATH,
  IN_MATH,
  CLS_WIN,
  VIM_WRT,
  DYNAMIC_MACRO_RANGE,
};

// enum unicode_names {
//     ALPHA,
//     BETA,
//     GAMMA,
//     DELTA,
//     CDELTA,
//     EPSILON,
//     MU,
//     SIGMA,
//     RHO,
//     NU,
//     CHI,
//     ANGS
// };

// // https://beta.docs.qmk.fm/features/feature_unicode
// // XP(key1, key2) to use key2 on shift
// const uint32_t PROGMEM unicode_map[] = {
//     [ALPHA]   = 0x1D770,  // α
//     [BETA]    = 0x1D771,  // β
//     [GAMMA]   = 0x1D772, // γ
//     [DELTA]   = 0x1D773,  // δ
//     [CDELTA]  = 0x1D71F,  // Δ
//     [EPSILON] = 0x1D774, // ε
//     [MU]      = 0x1D77B,  // μ
//     [SIGMA]   = 0x1D782,  // σ
//     [RHO]     = 0x1D780,  // ρ
//     [NU]      = 0x1D77C, // ν
//     [CHI]     = 0x1D7C0,  // χ 
//     [ANGS]    = 0x212B
// };

#define DYNAMIC_MACRO_SIZE 128
#include "dynamic_macro.h"

// These are defined to make them not mess up the grid.

#define SPC_SYM LT(_SYMBOLS, KC_SPC)
#define ENT_NUM LT(_NUMBERS, KC_ENT)
#define L_ARROW  MO(_ARROWS)
#define GUI_SFT SGUI_T(KC_DOT)
//#define ENT_GUI  LGUI(KC_ENT)
#define L_MEDIA MO(_MEDIA)
#define L_GREEK MO(_GREEK)
#define START1  DYN_REC_START1
#define PLAY1   DYN_MACRO_PLAY1
#define START2  DYN_REC_START2
#define PLAY2   DYN_MACRO_PLAY2
#define STOP    DYN_REC_STOP

// GREEK 
#define ALPHA   UC(0x03B1)  // α
#define BETA    UC(0x03B2)  // β
#define GAMMA   UC(0x03B3) // γ
#define DELTA   UC(0x03B4)  // δ
#define CDELTA  UC(0x0394)  // Δ
#define EPSILON UC(0x03B5) // ε
#define LMU     UC(0x03BC)  // μ
#define SIGMA   UC(0x03C3)  // σ
#define RHO     UC(0x03C1)  // ρ
#define NU      UC(0x03BD) // ν
#define CHI     UC(0x03C7)  // χ 
#define ANGS    UC(0x212B)


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [_QWERTY] = LAYOUT_arrow_command( /* Qwerty */
    KC_ESC,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_BSPC,
    KC_TAB,  KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_QUOT, KC_LGUI,
    KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, L_MEDIA,
    KC_LCTL, L_GREEK, KC_LALT, L_ARROW,          ENT_NUM, SPC_SYM,          IN_MATH, H_MATH,  PRTSCR,  MO(_RESET)
  ),


  [_NUMBERS] = LAYOUT_arrow_command( /* NUMBERS and F */
    _______, KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F10,  KC_F11,  KC_F12,  _______,
    _______, KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_LGUI,
    _______, XXXXXXX, KC_PLUS, KC_PMNS, XXXXXXX, XXXXXXX, XXXXXXX, KC_PEQL, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
    _______, _______, _______, XXXXXXX,          _______, GUI_SFT,          KC_LGUI, XXXXXXX, XXXXXXX, XXXXXXX
  ),


  [_SYMBOLS] = LAYOUT_arrow_command( /* SYMBOLS */
    VIM_WRT, KC_RABK, KC_RBRC, KC_RCBR, KC_RPRN, KC_SCLN, KC_AT,   KC_HASH, KC_TILD, KC_AMPR, XXXXXXX, _______,
    KC_BSLS, KC_LABK, KC_LBRC, KC_LCBR, KC_LPRN, KC_PIPE, KC_COLN, KC_SLSH, KC_CIRC, KC_DLR,  KC_ASTR, KC_PERC,
    KC_UNDS, XXXXXXX, KC_PLUS, KC_PMNS, KC_PEQL, XXXXXXX, KC_EXLM, KC_COMM, KC_GRV, KC_ASTR, KC_QUES, XXXXXXX,
    _______, _______, _______, PLAY1,            KC_DOT,  _______,          KC_AMPR, XXXXXXX, XXXXXXX, XXXXXXX
  ),


  [_GREEK] = LAYOUT_arrow_command( /* GREEK SYMBOLS and STUFF */
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
    XXXXXXX, GAMMA,   DELTA,   CDELTA,  EPSILON, LMU,     SIGMA,   RHO,     NU,      CHI,     ANGS,    XXXXXXX,
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,          ALPHA,   BETA,             XXXXXXX, XXXXXXX, XXXXXXX, _______
  ),


  [_ARROWS] = LAYOUT_arrow_command( /* ARROWS */
    _______, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, _______,
    _______, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT, XXXXXXX, _______,
    _______, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_UP,   XXXXXXX,
    _______, _______, _______, XXXXXXX,          XXXXXXX, KC_SPC,           XXXXXXX, KC_LEFT, KC_DOWN, KC_RGHT
  ),

  [_MEDIA] = LAYOUT_arrow_command( /* MEDIA */
    _______, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, _______,
    _______, XXXXXXX, KC_VOLU, KC_VOLD, KC_MUTE, XXXXXXX, XXXXXXX, XXXXXXX, KC_INS,  KC_DEL,  CLS_WIN, _______,
    _______, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, START2,  STOP,    PLAY2,   XXXXXXX,
    _______, _______, _______, XXXXXXX,          START1,  STOP,             XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX
  ),

  [_RESET] = LAYOUT_arrow_command( /* RESET */
    RESET,   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,          XXXXXXX, XXXXXXX,          XXXXXXX, XXXXXXX, XXXXXXX, _______
  )
};


// MACROS

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  if (!process_record_dynamic_macro(keycode, record)) {
         return false;
  }

  switch (keycode) {
    case PRTSCR:
      if (record->event.pressed) {
        SEND_STRING(SS_DOWN(X_LCTRL));
        SEND_STRING(SS_DOWN(X_PSCREEN));
        SEND_STRING(SS_DOWN(X_LSHIFT));
        SEND_STRING(SS_UP(X_LSHIFT));
        SEND_STRING(SS_UP(X_PSCREEN));
        SEND_STRING(SS_UP(X_LCTRL));
      }
      break;

      case MATH:
      if (record->event.pressed) {
        SEND_STRING("```math\n\n```" SS_TAP(X_LEFT)SS_TAP(X_LEFT)SS_TAP(X_LEFT)SS_TAP(X_LEFT));
      }
      break;

      case H_MATH:
      if (record->event.pressed) {
        SEND_STRING("```math\n\n" SS_TAP(X_LEFT));
      }
      break;

      case IN_MATH:
      if (record->event.pressed) {
        SEND_STRING("$``$" SS_TAP(X_LEFT)SS_TAP(X_LEFT));
      }
      break;

      case CLS_WIN:
      if (record->event.pressed) {
        SEND_STRING(SS_DOWN(X_LALT));
        SEND_STRING(SS_DOWN(X_F4));
        SEND_STRING(SS_UP(X_F4));
        SEND_STRING(SS_UP(X_LALT));
      }
      break;

      case VIM_WRT:
      if (record->event.pressed) {
        SEND_STRING(SS_DOWN(X_ESCAPE));
        SEND_STRING(SS_UP(X_ESCAPE));
        SEND_STRING(":w");
        SEND_STRING(SS_DOWN(X_ENTER));
        SEND_STRING(SS_UP(X_ENTER));


      }
      break;
  }
  return true;
};
