KC_HASH		         #
KC_SLSH              /
KC_DOT               .
KC_DLR               $
KC_CIRC	             ^
KC_TILD	             ~
KC_LPRN	             (
KC_RPRN	             )
KC_LCBR	             {
KC_RCBR	             }
KC_LBRC              [
KC_RBRC              ]
KC_LABK, KC_LT	     <
KC_RABK, KC_GT	     >
KC_COMM              ,
KC_GRV               `
KC_QUES	             ?
KC_PERC	             %
KC_EXLM	             !
KC_COLN	             :
KC_DQUO, KC_DQT      "
KC_PIPE              |
KC_BSLS              \
KC_UNDS              _
KC_PMNS              -
KC_PLUS              +
KC_AMPR              &
KC_ASTR              *
KC_AT                @
KC_PEQL              =