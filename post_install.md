# Apps and configuration

- Install all the relevant packages
- Set up configs

## YAY
[https://github.com/Jguer/yay]  

```
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
```

## PACMAN CONF
Open the pacman conf
`sudo vim /etc/pacman.conf`
Enable multilib
Add ILoveCandy under Misc Options

###
In sudoers file (`visudo`) under `Defaults specification` add:
```
Defaults timestamp_timeout=30
Defaults insults
```


## BASIC
ntfs-3g
openssh
git
vim
udevil
vlc
brave-bin
termite
feh
neofetch
picom
ttf-dejavu
deepin-screenshot
geeqie
xournalpp
emacs

### Notes

**Terminals**
URXVT uses .Xresources for its configuration  
Termite uses it own config file in ~/.config/termite/config  

**Misc**
deeping-screenshot uses keybinds configured in ~/.config/i3/config
ntfs-3g is for mounting ntfs partition as writable, without it they're mounted read only.

## WINDOW MANAGER
i3-gaps i3status i3blocks i3blocks-contrib  
dmenu  
sysstat acpi alsa-utils  
powerline nerd-fonts-complete

## LOGIN MANAGER

Choose one of the following.  

### Ly

[Go to repo for install instruction](https://github.com/cylgom/ly)

### Lightdm

lightdm  
lightdm-mini-greeter   
   
Lightdm uses the `.desktop` files in `/usr/share/xsessions`.    
In the lightdm-mini-greeter conf, make sure that the `user-session` matches one of those files (but without the extensions .desktop e.g `i3` if `i3.desktop`).  
This means that `.xinitrc` won't be loaded. Lightdm will execute `~/.xprofile` so all the commands (like the feh cmd for background) need to be put in `.xprofile` instead of `.xinitrc` (or in addition so we can remove lightdm with no hassle).  
Another method would be to add a `.desktop` in the above folder which would call the `.xinitrc`

## PROGRAMING
golang 
goland
gitfiend
atom
sublime
slack

### GO
When downloading the goland source, there's a script that allows to start it.  
Put the Goland folder in $HOME and rename it Goland (it should come with a name like GoLand-201803)  
Then set up a symlink like follow:  
`sudo ln -s $HOME/Goland/bin/goland.sh /usr/local/bin/goland`

## MISC
gimp2

## GITHUB and BITBUCKET
Look here for setting all accounts properly:  
https://medium.com/@fredrikanderzon/setting-up-ssh-keys-for-multiple-bitbucket-github-accounts-a5244c28c0ac  
**When prompt for email and user do not set global conf!!**
