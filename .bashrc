# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# --------------------------------------------------------------
#                          HISTORY
# --------------------------------------------------------------

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000


# --------------------------------------------------------------
#                            MISC
# --------------------------------------------------------------

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"


# --------------------------------------------------------------
#                            PS1
# --------------------------------------------------------------
parse_git_branch() {
     branch=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
		 if [[ ! -z $branch ]]
			 then  
			  echo -e "──[\e[1;33m$branch\e[1;37m]"
     fi
}

#PS1="\[\e[1;34m\]┌─[\[\e[33;1m\]\t\[\e[1;34m\]]──[\[\e[33;1m\]\u\[\e[1;34m\]]──[\[\e[33;1m\]\h\[\e[1;34m\]]:\[\e[0;36m\]\w \[\e[33;1m\]$\[\e[0m\]\n\[\e[01;34m\]└──|\[\e[0m\]"
PS1="\[\e[1;37m\]┌─[\[\e[1;36m\]\t\[\e[1;37m\]]──[\[\e[1;36m\]\u\[\e[1;37m\]]──[\[\e[1;36m\]\h\[\e[1;37m\]]\[\e[1;37m\]\$(parse_git_branch)\[\e[1;37m\]:\[\e[0;37m\]\w \[\e[36;1m\]$\[\e[0m\]\n\[\e[01;37m\]└──|\[\e[0m\]"


# --------------------------------------------------------------
#                          INCLUDES
# --------------------------------------------------------------
# if bash_aliases exists
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable bash completion in interactive shells
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

if [ -f ~/.ls_colors ]; then
	. ~/.ls_colors
fi


# --------------------------------------------------------------
#                          FUNCTIONS
# --------------------------------------------------------------
function gi(){
	echo $(gcloud compute instances list --filter=$1 | command grep -v NAME |cut -d " " -f1)
}

function geip(){
	echo $(gcloud compute instances list --filter=$1 | command grep -v NAME | tr -s " " |cut -d " " -f5)
}

function mdToPdf(){
  pandoc -s -V geometry:"top=1cm,bottom=2cm,left=1cm,right=1cm" --lua-filter /home/elinor/.standalone_markdown_preview/config/filters/gitlab-filter-pdf.lua -o $1 $2	
}

# --------------------------------------------------------------
#                           ALIASES
# --------------------------------------------------------------

alias ls='ls --color=auto'
alias grep='grep --colour=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias grep='grep -n --color=auto'  

alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias cd..='cd ..'
alias mkdir='mkdir -p'
alias myip='curl bot.whatismyipaddress.com'
alias bb='cd $GOPATH/src/bitbucket.org/stop-start'
alias gl='cd $GOPATH/src/gitlab.com/stop.start'
alias gosrc='cd $GOPATH/src'
alias smpc='smp --dir ~/Repos/Marine-Biology-BSc/Math_Prep/ -f math_prep.md --css https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/2.10.0/github-markdown.css'
alias keeb='cd $HOME/Repos/qmk_firmware/; sudo make thevankeyboards/minivan:elinor:dfu'
alias docsp_dev='docker run -it --name student-planner --rm -v ~/Repos/student-planner/:/student-planner/  -p 8080:8080 stop-start/student-planner /bin/sh'
alias docsp='docker run --name student-planner --rm -d -p 8080:8080 stop-start/student-planner'

# for work
alias got='cd $GOPATH/src/github.com/AudioStreamTV/'
alias mac='export GOOS=darwin GOARCH=amd64'
alias mon='xrandr --output DP1 --auto --output eDP1 --mode 2048x1152 --right-of DP1'
alias docdoc='sudo systemctl start docker.service'
alias dcker='docker'
alias dcoker='docker'
alias dcoekr='docker'
alias docekr='docker'
alias consul='docker run -d --name=dev-consul --rm -e CONSUL_BIND_INTERFACE=wlan0 --network host consul'
alias cli='./streamer-cli --audio-mode play --tuner 10105 --debug --port-http 8081 --ip $(geip tu-streamer-t-dev)'
alias stream='./streamer stream --external-ip localhost --session-service localhost:7777'
alias relay='./streamer relay'
alias gcp='gcloud compute scp'
alias gssh='gcloud compute ssh'

# --------------------------------------------------------------
#                        EXPORT
# --------------------------------------------------------------

#upup(){ DEEP=$1; [ -z "${DEEP}" ] && { DEEP=1; }; for i in $(seq 1 ${DEEP}); do cd ../; done; }
export EDITOR=vim
export GOPATH=~/Repos/godir
export PATH=$PATH:$GOPATH/bin


# --------------------------------------------------------------
#                        COMMANDS
# --------------------------------------------------------------
neofetch --w3m $HOME/Pictures/avatars/penguin.png --size 260px






source /usr/share/nvm/init-nvm.sh
