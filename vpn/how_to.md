# L2TP-IPSEC PSK VPN Client Configuration
*Used those links:*
https://www.elastichosts.com/blog/linux-l2tpipsec-vpn-client/
https://wiki.archlinux.org/index.php/Openswan_L2TP/IPsec_VPN_client_setup


## Dependencies
xl2tpd
openswan

## Before starting
** Start openswan **
sudo systemctl start openswan

## IPSec

### Copy the config files
Copy the content of **ipsec.conf** in **/etc/ipsec.conf**.
Change the details in *<To change>* and save.

Copy the content of **ipsec.secrets** in **/etc/ipsec.secrets**
Change the details in *<To change>* and save

### Add the connectio so it's available to use:
`ipsec auto --add L2TP-PSK`

## xl2tpd

### Copy the config files
Copy the content of **xl2tpd.conf** in **/etc/xl2tpd/xl2tpd.conf**.
Change the details in *<To change>* and save.

Copy the content of **options.l2tpd.client** in **/etc/ppp/options.l2tpd.client**.
Change the user and password and save.

### Create the control file for xl2tpd
```
mkdir -p /var/run/xl2tpd
touch /var/run/xl2tpd/l2tp-control
```

## Connect

```
systemctl restart openswan
systemctl start xl2tpd
ipsec auto --up L2TP-PSK
echo "c work-vpn" > /var/run/xl2tpd/l2tp-control
``` 

Check in `ip addr` that `pppX` appears.


## Routing all traffic
```
ip route add <SERVER IP> via <HOME/CURRENT GATEWAY> dev <INTERFACE>
ip route add default via <VPN GATEWAY> dev <INTERFACE>
ip route delete default via <HOME/CURRENT GATEWAY> dev <INTERFACE>

```

## Shutdown
```
ipsec auto --down L2TP-PSK
echo "d work-vpn" > /var/run/xl2tpd/l2tp-control
systemctl stop xl2tpd stop
systemctl stop openswan
ip route del <SERVER IP> via <HOME/CURRENT GATEWAY> dev <INTERFACE>
ip route add default via <HOME/CURRENT GATEWAY> 
```


