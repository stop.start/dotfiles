"""""""""""""""""""""""""""""""""""""""""""""""""""""
"My vimrc file
"""""""""""""""""""""""""""""""""""""""""""""""""""""

syntax on

"""""""""""""""""""""""""""""""""""""""""""""""""""""
"=> Colors & Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""

"------------------------------------
"colorschemes that works with terminal
"------------------------------------
"colorscheme lucius
"LuciusLight
"colorscheme calmar256-dark
colorscheme leo
"colorscheme inkpot
"colorscheme lettuce
"colorscheme xoria256
"colorscheme desert256
"colorscheme dante
"colorscheme vividchalk
"colorscheme 256-grayvim
"colorscheme burnttoast256
"colorscheme 256-jungle
"colorscheme herald
"colorscheme mayansmoke
"colorscheme soso
"------------------------------------
"colorschemes GUI
"------------------------------------
if has("gui_running")
colorscheme blackboard
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""
"=> Tab & Indent
"""""""""""""""""""""""""""""""""""""""""""""""""""""
" Indent = 2
set shiftwidth=2
set tabstop=2
set smartindent

"""""""""""""""""""""""""""""""""""""""""""""""""""""
"=> Line numbers & Map
"""""""""""""""""""""""""""""""""""""""""""""""""""""
set number
nmap <C-N><C-N> :set invnumber <CR>

