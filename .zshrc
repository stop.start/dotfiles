# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/elinor/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="bullet-train"
BULLETTRAIN_PROMPT_CHAR=">"
BULLETTRAIN_TIME_BG=#ffffff
BULLETTRAIN_STATUS_BG=#99cc99
BULLETTRAIN_STATUS_ERROR_BG=#f2777a
BULLETTRAIN_STATUS_FG=#ffffff
BULLETTRAIN_DIR_BG=#99cc99
BULLETTRAIN_DIR_FG=#2d2d2d
BULLETTRAIN_CONTEXT_BG=#6699cc
BULLETTRAIN_CONTEXT_FG=#ffffff
BULLETTRAIN_GO_BG=#66cccc
BULLETTRAIN_NVM_BG=#f99157
BULLETTRAIN_NVM_FG=#ffffff

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# --------------------------------------------------------------
#                          FUNCTIONS
# --------------------------------------------------------------

function mdToPdf(){
  pandoc -s -V geometry:"top=1cm,bottom=2cm,left=1cm,right=1cm" --lua-filter /home/elinor/.standalone_markdown_preview/config/filters/gitlab-filter-pdf.lua -o $1 $2
}

# --------------------------------------------------------------
#                           ALIASES
# --------------------------------------------------------------

alias ls='ls --color=auto'
alias grep='grep --colour=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias grep='grep -n --color=auto'

alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias cd..='cd ..'
alias mkdir='mkdir -p'
alias myip='curl bot.whatismyipaddress.com'
alias bb='cd $GOPATH/src/bitbucket.org/stop-start'
alias gl='cd $GOPATH/src/gitlab.com/stop.start'
alias gosrc='cd $GOPATH/src'
alias smpc='smp --dir ~/Repos/Marine-Biology-BSc/Math_Prep/ -f math_prep.md --css https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/2.10.0/github-markdown.css'
alias keeb='cd $HOME/Repos/qmk_firmware/; sudo make thevankeyboards/minivan:elinor:dfu'
alias mon='xrandr --output DP1 --auto --output eDP1 --mode 2048x1152 --right-of DP1'

alias docsp_dev='docker run -it --name student-planner --rm -v ~/Repos/student-planner/:/student-planner/  -p 8080:8080 stop-start/student-planner /bin/sh'
alias docsp='docker run --name student-planner --rm -d -p 8080:8080 stop-start/student-planner'
alias docdoc='sudo systemctl start docker.service'
alias dcker='docker'
alias dcoker='docker'
alias dcoekr='docker'
alias docekr='docker'


# --------------------------------------------------------------
#                        EXPORT
# --------------------------------------------------------------

export EDITOR=vim
export GOPATH=~/Repos/godir
export PATH=$PATH:$GOPATH/bin


# --------------------------------------------------------------
#                        COMMANDS
# --------------------------------------------------------------
neofetch --w3m $HOME/Pictures/avatars/penguin.png --size 260px


