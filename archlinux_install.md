# Arch linux install  

This guide is meant for myself and is tailored to my current hardware configuration.    
Note: Written after the install, might contain some inaccuracies.    
Arch linux install on desktop with 2 SSDs and one HDD, with UEFI.    
One SSD with Windows, HDD for Windows data.    
One entire SSD for linux (no dual OS)    
Prepare bootable usb with arch linux.    
For more safety, disconnect the Windows drives (both of them).    

## Basic install  
[https://wiki.archlinux.org/index.php/Installation_guide]  

### Verify UEFI mode by checking that the following directory exists
`ls /sys/firmware/efi/efivars`  

### Connect to internet  
[https://wiki.archlinux.org/index.php/Wireless_network_configuration]   
*Wired: no need to do anything (just check ping)*  
**Wireless:**  
Find name of interface:  
`iw dev`  
Activate interface:  
`ip link set <interface> up`  

> If there's an _rfkill_ error:  
> `echo "blacklist hp_wmi" > /etc/modprob.d/hp.conf`  
> `rfkill unblock all`  
> Try again to activate interface  

Check that it’s up:  
`ip link show <interface>` : in the `<BROADCAST…, … UP>` → UP shows that it’s up  
WPA2 connection:  
`wpa_supplicant -B -i interface -c <(wpa_passphrase <SSID> <passphrase>)`  
Get ip from DHCP:  
`dhcpd <interface>`  
Check ping.  

### Update clock
`timedatectl set-ntp true`  

### Partition disk (uefi mode)
[https://wiki.archlinux.org/index.php/Fdisk#gdisk]  
>For 120GB SSD the plan is:  
/boot/efi → 1GB  
/ → 22GB  
Swap → 8 GB  
/home → what’s left  

`lsblk` to check device it is (/dev/sdX)  

**Enter gdisk/fdisk**  
`gdisk /dev/sdX`  
*n is for new partition, use the default start sector, then for example +22G for 22gb.*

>Codes:  
UEFI: ef00  
SWAP: 8200  
/ and /home should be default (linux fs)  

**Order: uefi → / → swap → /home**  
Finish by writing with ‘w’.  
*/dev/sdX1 should be uefi boot partition  
/dev/sdX2 should be /  
/dev/sdX3 should be swap  
/dev/sdX4 should be /home**

**Format the partitions:**  
For UEFI (**not for dualboot!!**):  
`mkfs.fat -F32 /dev/sdX1`  
For / and /home:  
`mkfs.ext4 /dev/sdX2`  
`mkfs.ext4 /dev/sdX4`  
For SWAP:  
`mkswap /dev/sdX3`  
`swapon /dev/sdX3`  

**Mount the file systems:**  
Mount / (create /mnt if needed):  
`mount /dev/sdX2 /mnt`  

Prepare the directories:
`mkdir -p /mnt/boot/efi`  
`mkdir /mnt/home`  

Mount /boot/efi:  
`mount /dev/sdX1 /mnt/boot/efi`  
Mount /home:  
`mount /dev/sdX4 /mnt/home`  

### Optional: Edit Mirror list
`vim /etc/pacman.d/mirrorlist`  

### Install archlinux
`pacstrap /mnt base base-devel linux linux-firmware`  

### FSTAB
`genfstab -U /mnt >> /mnt/etc/fstab`   

### Chroot
`arch-chroot /mnt`  

### Timezone
`ln -sf /usr/share/zoneinfo/Israel /etc/localtime`  

### Time
`hwclock --systohc`

### Locale
Uncomment from /etc/locale.gen:  
en_US.UTF-8 UTF8  
he_…  
Then:  
`locale-gen`  
In /etc/locale.conf  
`LANG=en_US.UTF-8`  

### Hostname
vim /etc/hostname  
then : myhostname  
vim /etc/hosts  
127.0.1.1 myhostname.localdomain myhostname  

### Misc
If wireless install the packages:  
`pacman -S iw wpa_supplicant`  
Optional but recommended install  connection manager for example ConnMan:  
`pacman -S connman bluez openvpn`  
Note: bluez is for bluetooth and openvpn for vpn  
Explanation on use after reboot  
Set root password:  
`passwd`  
For intel CPU:  
`pacman -S intel-ucode`  

### Install and config boot loader (GRUB)
[https://wiki.archlinux.org/index.php/GRUB#Check_if_you_have_GPT_and_an_ESP]    
Check GPT and ESP  
parted /dev/sdx print (if parted not installed install it)  
Should see “Partition Table: gpt” as well as the UEFI partition  

**Install grub:**  
`pacman -S grub efibootmgr os-prober
`  
Then:  
`grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=grub`  
Generate conf file:  
`grub-mkconfig -o /boot/grub/grub.cfg`  

*Note: For windows partition we’ll regenerate the file after reconnecting the SSD*


#### Grub background and font size(optional)
[https://wiki.archlinux.org/index.php/GRUB/Tips_and_tricks#Background_image_and_bitmap_fonts]  
In order to have a custom background in Grub:  
  - Copy the image to /boot/grub/
  - Set the `GRUB_BACKGROUND` option in `/etc/default/grub`

*Beware that not all resolutions work and so not all images will show*  

Increasing the font size:
  - Create grub font form available font: `sudo grub-mkfont --output=/boot/grub/fonts/DejaVuSansMono28.pf2 --size=28 /usr/share/fonts/TTF/DejaVuSansMono.ttf`
  - Add the new created from by adding `GRUB_FONT=...` option in `/etc/default/grub`

Regenerate the file:
`grub-mkconfig -o /boot/grub/grub.cfg`

### Reboot  
exit  
reboot  

## Post basic install
Remove usb bootable  
Check that arch linux starts  
Power off  
Plug back the SDD and HDD.  
Start the machine.  
Login (root should be fine for the following)  

### Get GRUB to add Windows in the menu by adding a grub menu entry
[https://wiki.archlinux.org/index.php/GRUB#Windows_installed_in_UEFI-GPT_Mode_menu_entry]  
Identify the device where windows is with  
`lsblk`  
Find which partition has the uefi boot (maybe fdsik -l can be handy)  
Mount this partition at /boot/efi:  
`mount /dev/sdXY /boot/efi`  
Then do and keep the output of:  
`grub-probe --target=fs_uuid /boot/efi/EFI/Microsoft/Boot/bootmgfw.efi`  
Same with:  
`grub-probe --target=hints_string /boot/efi/EFI/Microsoft/Boot/bootmgfw.efi`

Write the following in **/boot/grub/custom.cfg** where **$fs_uuid** is the result of the first command (replace it in the code) and **$hints_string** is the result of the second command (replace it as well)  

```
if [ "${grub_platform}" == "efi" ]; then  
	menuentry "Microsoft Windows Vista/7/8/8.1/10 UEFI-GPT" {  
		insmod part_gpt  
		insmod fat  
		insmod search_fs_uuid  
		insmod chain  
		search --fs-uuid --set=root $hints_string $fs_uuid  
		chainloader /EFI/Microsoft/Boot/bootmgfw.efi  
	}  
fi
```  

Regenerate grub config file (it should see the windows partition):  
`grub-mkconfig -o /boot/grub/grub.cfg`  
*If nothing has changed try to use os-prober and redo the command*  

Reboot to check that all is ok (and try Windows as well)  

### If windows EFI partition was accidentally erased (dualboot)
[Download tool](https://www.microsoft.com/en-us/software-download/windows10) to create an installation usb for windows10 from a windows10 computer.  
Click _troubleshoot_, then _Advanced otions_, then _Command Prompt_.  

```
diskpart  
sel disk 0
list vol  
```

The UEFI volume should be around 260MB (if unsure go to linux and use fdisk -l).  Select it with:
```
select vol NumOfVolume
```
Assign (an unused) letter for UEFI volume:  

```
assign letter=X
```
Then:  

```
exit
mkdir X:\EFI\Microsoft\Boot\
xcopy /s C:\Windows\Boot\EFI\*.* X:\EFI\Microsoft\Boot
cd /d X:\EFI\Microsoft\Boot\
bcdboot C:\windows /s X: /f UEFI
```


### Create user
`useradd -m -G wheel -s /bin/bash username`  
Password:  
`passwd username`  
Use visudo to add user to the sudo group
Switch user  

### Internet configuration with Connman
[https://wiki.archlinux.org/index.php/ConnMan]  
Enable and start service:  
`sudo systemctl enable connman.service`  
`sudo systemctl start connman.service`  

Wifi:  
```
connmanctl enable wifi
connmanctl  
connmanctl> scan wifi
connmanctl> services  
connmanctl> agent on  
connmanctl>connect wifi_xxxxx_xxxxx_managed_psk  
```
*wifi_blahblah from scan wifi.*  
Enter passphrase when prompt  
`connmanctl>quit`  
Connman will connect automatically to the known wifi on startup.  

### Graphics drivers and Xorg
[https://wiki.archlinux.org/index.php/Xorg]  
Install xorg group and xorg-xinit:  
`pacman -S xorg xorg-xinit`  
Install graphics driver. For example, for open source Nvidia:  
`pacman -S xf86-video-nouveau lib32-mesa`

## Finish
TODO: Complete list of apps  
Install programs and window manager (like i3)  
Restore bashrc/Xresources/vimrc etc..  
Note: the resolution in set in /etc/X11/xorg.org/10-monitor.conf
